
import { Component, NgModule, NgZone, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NavParams, ViewController } from "ionic-angular";

import { FormControl, FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { MapsAPILoader } from 'angular2-google-maps/core';

import {
  GoogleMap
} from 'ionic-native';

import { Location } from "../../models/location";

declare var google: any;

@Component({
  selector: 'page-set-location',
  templateUrl: 'set-location.html'
})
export class SetLocationPage implements OnInit {

  
  private map: GoogleMap;

  public latitude: number;
  public longitude: number;
  public searchControl: FormControl;
  public zoom: number;

  @ViewChild("search")
  public searchElementRef: ElementRef;

  location: Location;
  marker: Location;

  constructor(private navParams: NavParams,
              private viewCtrl: ViewController,
              
              private mapsAPILoader: MapsAPILoader,
              private ngZone: NgZone
              
              ) {
    this.location = this.navParams.get('location');
    if (this.navParams.get('isSet')) {
      this.marker = this.location;
    }
  }

  onSetMarker(event: any) {
    console.log(event);
    this.marker = new Location(event.coords.lat, event.coords.lng);
  }

  onConfirm() {
    this.viewCtrl.dismiss({location: this.marker});
  }

  onAbort() {
    this.viewCtrl.dismiss();
  }










 
  ngOnInit() {
    //set google maps defaults
    this.zoom = 4;
    this.latitude = 39.8282;
    this.longitude = -98.5795;
    
    //create search FormControl
    this.searchControl = new FormControl();

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {

          //get the place result
          //let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          let place = autocomplete.getPlace();
  
          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          console.log("place", place);
          
          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;


          this.marker = new Location(this.latitude, this.longitude);




          this.map = new GoogleMap("map-canvas");

  this.map = new google.maps.Map(document.getElementById('map'), {
    center: this.marker,
    zoom: 15
  });

          console.log("this.map",this.map);


          console.log("marker",this.marker);


          var service = new google.maps.places.PlacesService(this.map);
          service.nearbySearch({
            location: this.marker, 
            radius: 500,
            types: ['store','cine','comida']
          }, this.callback);





        });
      });
    });



  }



    callback(results, status) {


          console.log("results", results);
          
      /*
      if (status === google.maps.places.PlacesServiceStatus.OK) {
        for (var i = 0; i < results.length; i++) {
          this.createMarker(results[i]);
        }
      }*/
    }

    createMarker(place) {
      var placeLoc = place.geometry.location;
      var marker = new google.maps.Marker({
        map: this.map,
        position: place.geometry.location
      });



    }



}
